from rest_framework import serializers
from .models import Companies, CompanyCategoryGroup, DealerCompanyGroup


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Companies
        fields = '__all__'


class CompanyCategoryGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyCategoryGroup
        fields = '__all__'


class DealerCompanyGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = DealerCompanyGroup
        fields = '__all__'
