from django.urls import path
from .views import CompanyViewSet, CompanyCategoryViewSet, DealerCompanyViewSet

urlpatterns = [
    path('companies', CompanyViewSet.as_view({
        'get': 'list',
        'post': 'create'
    })),
    path('companies/<str:pk>', CompanyViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'delete': 'destroy'
    })),
    path('category', CompanyCategoryViewSet.as_view({
        'post': 'create',
        'get': 'list'
    })),
    path('category/<str:pk>', CompanyCategoryViewSet.as_view({
        'put': 'update',
        'delete': 'destroy',
        'get': 'retrieve'
    })),
    path('companies/dealer', DealerCompanyViewSet.as_view({
        'post': 'create',
        'get': 'list'
    })),
    path('companies/dealer/<str:pk>', DealerCompanyViewSet.as_view({
        'put': 'update',
        'delete': 'destroy',
        'get': 'retrieve'
    }))
]