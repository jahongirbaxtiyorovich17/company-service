from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from .models import Companies, CompanyCategoryGroup, DealerCompanyGroup
from .serializers import CompanySerializer, CompanyCategoryGroupSerializer, DealerCompanyGroupSerializer


class CompanyViewSet(viewsets.ViewSet):
    def list(self, request):
        companies = Companies.objects.all()
        serializers = CompanySerializer(companies, many=True)
        return Response(serializers.data)

    def create(self, request):
        serializer = CompanySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        companies = Companies.objects.get(company_id=pk)
        serializer = CompanySerializer(companies)
        return Response(serializer.data)

    def update(self, request, pk=None):
        companies = Companies.objects.get(company_id=pk)
        serializer = CompanySerializer(instance=companies, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def destroy(self, request, pk=None):
        companies = Companies.objects.get(company_id=pk)
        companies.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CompanyCategoryViewSet(viewsets.ViewSet):
    def list(self, request):
        company_category = CompanyCategoryGroup.objects.all()
        serializers = CompanyCategoryGroupSerializer(company_category, many=True)
        return Response(serializers.data)

    def create(self, request):
        serializer = CompanyCategoryGroupSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, pk=None):
        company_category = CompanyCategoryGroup.objects.get(id=pk)
        serializer = CompanyCategoryGroupSerializer(instance=company_category, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def retrieve(self, request, pk=None):
        company_category = CompanyCategoryGroup.objects.get(id=pk)
        serializer = CompanySerializer(company_category)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        company_category = CompanyCategoryGroup.objects.get(id=pk)
        company_category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DealerCompanyViewSet(viewsets.ViewSet):
    def list(self, request):
        dealer_company = DealerCompanyGroup.objects.all()
        serializers = DealerCompanyGroupSerializer(dealer_company, many=True)
        return Response(serializers.data)

    def create(self, request):
        serializer = DealerCompanyGroupSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, pk=None):
        dealer_company = DealerCompanyGroup.objects.get(id=pk)
        serializer = DealerCompanyGroupSerializer(instance=dealer_company, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def retrieve(self, request, pk=None):
        dealer_company = DealerCompanyGroup.objects.get(id=pk)
        serializer = DealerCompanyGroupSerializer(dealer_company)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        dealer_company = DealerCompanyGroup.objects.get(id=pk)
        dealer_company.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

