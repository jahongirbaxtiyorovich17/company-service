import datetime
from django.db import models

# Models are created here.

'''
Class below creates models for Companies table in database
'''


class Companies(models.Model):
    company_id = models.IntegerField(primary_key=True)
    parent_id = models.IntegerField()
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=200)
    options = models.TextField(max_length=200)
    logo = models.CharField(max_length=500)
    status = models.IntegerField()
    created_at = models.DateTimeField(default=datetime.date.today)
    updated_at = models.DateTimeField(default=datetime.date.today)


''' 
Class below creates many to many relationship of company table with category table
Category table will be connected this class through RabbitMQ
'''


class CompanyCategoryGroup(models.Model):
    category_group_id = models.IntegerField(primary_key=True)
    company_id = models.ForeignKey(Companies, on_delete=models.CASCADE)
    category_id = models.IntegerField()
    active = models.IntegerField(default=0)


'''
Class below represents connection between Dealer table and Companies table
'''


class DealerCompanyGroup(models.Model):
    dealer_group_id = models.IntegerField(primary_key=True)
    company_id = models.ForeignKey(Companies, on_delete=models.CASCADE)
    dealer_id = models.IntegerField()
